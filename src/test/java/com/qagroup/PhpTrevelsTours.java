package com.qagroup;

import java.sql.Driver;
import java.time.LocalDate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.app.DatePicker;
import com.qagroup.phptravels.app.TourDetailsPage;
import com.qagroup.phptravels.app.ToursPage;



public class PhpTrevelsTours {
	
	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	
	private WebDriver driver;
		
	private ToursPage toursPage;

	private TourDetailsPage tourDetailsPage;

	private DatePicker datePicker;
	
	
	
		
	@Test    
	 public void PhpTrevelsToursTest() {
     toursPage = phpTravelsApp.openToursPage();
	 waitFor(3);
     toursPage.clickdetails();
     waitFor(3);
     
     tourDetailsPage = toursPage.clickdetails();
   
      Assert.assertTrue(tourDetailsPage.isMainPhotoDisplayed());
      
      Assert.assertTrue(tourDetailsPage.isMainDetailsDisplayed());
      
      

}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}
	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}


