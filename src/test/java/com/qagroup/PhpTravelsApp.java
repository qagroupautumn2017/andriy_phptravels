package com.qagroup;

import org.openqa.selenium.WebDriver;

import com.qagroup.phptravels.app.MainPage;
import com.qagroup.phptravels.app.ToursPage;

import phptravels.net.tours.tools.Browser;

public class PhpTravelsApp {
	
	private WebDriver driver;
	public PhpTravelsApp() {
		
	}
   public  ToursPage openToursPage() { 
	   driver = Browser.open();
       driver.get("http://www.phptravels.net/tours");
	   return new ToursPage(driver);
	   }

   public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
    }
  }
}
