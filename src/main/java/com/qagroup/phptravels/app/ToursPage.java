package com.qagroup.phptravels.app;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ToursPage {
	private WebDriver driver;
	@FindBy(css = ".btn.btn-action:not(#searchform)")
	private WebElement detailsButton;
	
	

	public ToursPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}
	
	public TourDetailsPage clickdetails() {
	detailsButton.click();
	return new TourDetailsPage(driver);
	}

}
