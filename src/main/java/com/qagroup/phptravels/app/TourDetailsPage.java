package com.qagroup.phptravels.app;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.qameta.allure.Step;

public class TourDetailsPage {
	
	@FindBy(css = ".fotorama__active.fotorama__stage__frame")
	private WebElement mainPhoto;
	
	@FindBy(css = ".panel-green")
	private WebElement mainDetails;
	
	@FindBy(css = ".datepicker[style*='display: block']")
	private WebElement datePicker;
	
	@FindBy(css = "[name='checkin']")
	private WebElement checkInInput;
	
	private WebDriver driver;
	
	public TourDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public DatePicker datePicker() {
		return new DatePicker(datePicker);
		}
		
		@Step("Open 'Check In' date picker")
		public DatePicker openTuorCheckInDatePicker() {
			checkInInput.click();
			DatePicker picker = datePicker();
			return picker;
	}

	

	public  boolean isMainPhotoDisplayed() {
		return mainPhoto.isDisplayed();
	}
	
	public  boolean isMainDetailsDisplayed() {
		return mainDetails.isDisplayed();
	}
}


